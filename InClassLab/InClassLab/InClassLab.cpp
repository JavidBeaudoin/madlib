#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

const string path = "C:\\temp\\test.txt";

void PrintIntArray(string *pArray, int SIZE, ostream &ofs);

/*
void PrintIntArray(string *pArray, int SIZE, ostream &ofs)
{
	std::cout << "One day my " << pArray[0] << " friend and i decided to go to the " << pArray[1] << " game in " << pArray[2] << ".\n" << "we really wanted to see " << pArray[3] << " play." << "\n" << "so we skipped in the " << pArray[4] << " and headed down to " << pArray[5] << " and bought some " << pArray[6] << "." << "\n" << "we watched the game and it was " << pArray[7] << "." << "\n" << "we ate some " << pArray[8] << " and drank some " << pArray[9] << "." << "\n" << "We had a " << pArray[10] << " time, and cant wait to go again.";

}
*/
int main()
{
	string adjective;

	const int SIZE = 12;
	string pArray[SIZE] = { "enter an adjective (describing word): " , "Enter a sport: ", "Enter a city name: ", "enter a person: ", "Enter an action verb (past tense): ", "Enter a vehicle: ", "Enter a place: ", "enter a noun (thing, plural): ", "Enter an adjective (describing word):  ", "Enter a food: ", "Enter a liquid: " , "Enter an adjective (descriving word): " };
	
	for (int i = 0; i < SIZE; i++)
	{
		cout << pArray[i];
		getline(cin, pArray[i]);
	}

	PrintIntArray(pArray, SIZE, cout);
	
	ofstream ofs;
	ofs.open(path);

	PrintIntArray(pArray, SIZE, cout);
	ofs.close();

	cout << "Reading from file: \n";

	ifstream ifs(path);
	string line;
	while (getline(ifs, line)) 
	{
		cout << line << "\n";

	}

	ifs.close();

	//pArray[0];
	_getch();
	return 0;
}

void PrintIntArray(string *pArray, int SIZE, ostream &os)
{
	std::cout << "One day my " << pArray[0] << " friend and i decided to go to the " << pArray[1] << " game in " << pArray[2] << ".\n" << "we really wanted to see " << pArray[3] << " play." << "\n" << "so we skipped in the " << pArray[4] << " and headed down to " << pArray[5] << " and bought some " << pArray[6] << "." << "\n" << "we watched the game and it was " << pArray[7] << "." << "\n" << "we ate some " << pArray[8] << " and drank some " << pArray[9] << "." << "\n" << "We had a " << pArray[10] << " time, and cant wait to go again.";
	
}
